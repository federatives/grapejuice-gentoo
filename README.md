## Deprecation Notice

This overlay has been deprecated in favor of my other overlay ::dietnusse. Not only does it contain a completely rewritten ebuild for Grapejuice, but it also includes some other packages as well.
