# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

IUSE="pulseaudio"

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..10} )

inherit distutils-r1

DESCRIPTION="A Wine+Roblox management tool"
HOMEPAGE="https://gitlab.com/brinkervii/grapejuice"

SRC_URI="https://gitlab.com/brinkervii/grapejuice/-/archive/v${PV}/grapejuice-v${PV}.zip"
S="${WORKDIR}/${PN}-v${PV}"

LICENSE="GPL-3+"
SLOT="0"

KEYWORDS="-* ~amd64"

RDEPEND="
		>=x11-libs/gtk+-3.24.34
		net-libs/gnutls[abi_x86_32]
		pulseaudio? ( media-libs/libpulse[abi_x86_32] )
		sys-devel/gettext
		dev-python/pip
		x11-libs/cairo
		dev-libs/gobject-introspection
		dev-util/desktop-file-utils
		x11-misc/xdg-utils
		x11-misc/xdg-user-dirs
		dev-util/gtk-update-icon-cache
		x11-misc/shared-mime-info
		x11-apps/mesa-progs
		dev-python/psutil
		dev-python/click
		dev-python/requests
		dev-python/unidecode
"
DEPEND="${RDEPEND}"
BDEPEND="app-arch/unzip"

distutils_enable_tests pytest

# (hacky) workaround for locales and desktop files.
# TODO: Look for a better way to do this.
python_install() {
	distutils-r1_python_install
	mkdir -vp ${D}/usr/share/locale/{ro,es}/LC_MESSAGES || die
	msgfmt ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/po/es.po -o ${D}/usr/share/locale/es/LC_MESSAGES/grapejuice.mo || die
	msgfmt ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/po/ro.po -o ${D}/usr/share/locale/ro/LC_MESSAGES/grapejuice.mo || die
	mkdir -vp ${D}/usr/share/applications || die
	# Why didn't Brinker just use these replacements
	# instead of these stupid variables?
	sed -i -e 's/\$\GRAPEJUICE_ICON/grapejuice.svg/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/grapejuice.desktop || die
	sed -i -e 's/\$\GRAPEJUICE_GUI_EXECUTABLE/grapejuice-gui/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/grapejuice.desktop || die
	sed -i -e 's/\$\PLAYER_ICON/grapejuice-roblox-player.svg/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-app.desktop || die
	sed -i -e 's/\$\GRAPEJUICE_EXECUTABLE/grapejuice/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-app.desktop || die
	sed -i -e 's/\$\PLAYER_ICON/grapejuice-roblox-player.svg/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-player.desktop || die
	sed -i -e 's/\$\GRAPEJUICE_EXECUTABLE/grapejuice/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-player.desktop || die
	sed -i -e 's/\$\STUDIO_ICON/grapejuice-roblox-studio.svg/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-studio.desktop || die
	sed -i -e 's/\$\GRAPEJUICE_EXECUTABLE/grapejuice/' ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/roblox-studio.desktop || die
	cp ${D}/usr/lib/${EPYTHON}/site-packages/grapejuice_common/assets/desktop/{roblox-app.desktop,roblox-player.desktop,roblox-studio.desktop,grapejuice.desktop} ${D}/usr/share/applications || die
}

pkg_postinst() {
	elog "To use Roblox, make sure to install Wine."
	elog "Use Wine from either the prebuilt Python script at"
	elog "https://brinkervii.gitlab.io/grapejuice/docs/Guides/Installing-Wine.html"
	elog "or install Wine from the repository itself."
}
